## Technology used in this project
- Spring Boot
- Hibernate (Spring data jpa)
- Spring Security (We will use BCryptPasswordEncoder for password encoding and decoding)
- MySQl RDBMS
- Maven for builds
- Thymeleaf for simple UI


## Spring initializr project details

![](/Spring_initializr.JPG)

## How to run and test this project?

- After opening gitpod snapshot link. If you get message for "Confiugure Java Runtime", on the terminal run command _'sdk install java'_.
- Set the java 17.0.3-tem as default. Enter 'Y'
- Run command _'mvn clean install'_.
- Run _'docker-compose up -d'_ . Wait for couple of minutes for the containers to be up and running.

![](/Docker_Compose_cmd.jpg)

- It will create new image for both the services(app and database).
- Two new containers will start, one for the app and the other for MySQL database.
- Docker compose file has defined database name 'demo' which will get created when database container is starting.
- Hibernate will generate database tables automatically(defined in the model) once application is connected to the database.
- To check the tables within MySQL database server on the container, open MySQL workbench and provide connection details as mentioned below.

![](/MySQL_Connection.jpg)

- From the gitpod menu bar, Remote Explorer, open port 8080 in the browser.
- APIs developed can be tested using the UI created. It is very self explanatory.
- Sign up User's First Name, Last Name, Email and Password.
- Login with the Email and password.
- Try to Logout.
- To stop the application use command *'docker-compose down'*


## Example REST endpoints

- [ ] POST - https://8080-janamshah-springbootres-g1w2k7ve3bm.ws-us54.gitpod.io/registration
- [ ] POST - https://8080-janamshah-springbootres-g1w2k7ve3bm.ws-us54.gitpod.io/login
- [ ] GET - https://8080-janamshah-springbootres-g1w2k7ve3bm.ws-us54.gitpod.io/logout

![](/Signup.jpg)

![](/login.jpg)

![](/login_to_app.jpg)

![](/logout.jpg)
