FROM openjdk:8-jdk-alpine
EXPOSE 8080
ADD target/signup-login-logout-janam.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]